--[[
	Suneil Nyamathi
	snyamathi@gmail.com
]]

-- Add each input as a key to a table...
local input = {}
for count = 1,5 do
	io.write("Enter a number: ")
	input[io.read()] = true
end

io.write("Enter the number to be searched: ")
local key = io.read()

-- So that we can try to find it later
if input[key] then
	print("The number " .. key .. " appears in the first 5 numbers.")
else 
	print("The number " .. key .. " does not appear in the first 5 numbers.")
end