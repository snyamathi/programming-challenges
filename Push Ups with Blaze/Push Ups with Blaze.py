'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

score = int(input("Enter final score: "))

# Just to clarify that we're using a finite series
n = score/7
pushups = 7 * n*(n+1)/2

print ("Push ups: %d" % pushups)