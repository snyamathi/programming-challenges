/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

int main()
{
	long score;
	std::cout << "Enter final score: ";
	std::cin >> score;

	// Just to clarify that we're using a finite series
	int n = score/7;

	int pushups = 7 * n*(n+1)/2;
	std::cout << "Push ups: " << pushups << std::endl;
	return 0;
}