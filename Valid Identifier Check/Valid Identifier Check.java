/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Class {

	public static void main(String[] args) {	
		System.out.print("Enter id: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String id = br.readLine().trim();
			if (isValid(id)) {
				System.out.println(id + " is a valid identifier");
			} else {
				System.out.println(id + " is not a valid identifier");
			}
		} catch (IOException e) {
			// Not concerned with errors for these challenges unless required
		}
	}

	private static boolean isValid(String id) {
		// Must start with a character
		if (!Character.isLetter(id.charAt(0)))
			return false;
		
		// Cannot end with a _
		if (id.charAt(id.length()-1) == '_')
			return false;
		
		// Cannot have two _ in a row, see below for $
		if (id.contains("__") || id.contains("$"))
			return false;
		
		// Take advantage of a handy function, but we need to restrict the use
		// of $ which is not allowed here, but is allowed in Java identifiers
		for (char c : id.toCharArray()) {
			if (!Character.isJavaIdentifierPart(c))
				return false;
		}
		
		return true;
	}
}
