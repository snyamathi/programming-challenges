/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

public class FizzBuzz {
	
	public static void main(String[] args) {
		
		boolean div3;
		boolean div5;
		
		for (int i = 1; i <= 100; i++) {
			div3 = (i%3) == 0;
			div5 = (i%5) == 0;
			
			if (div3 && div5) {
				System.out.println("FizzBuzz");
			} else if (div3) {
				System.out.println("Fizz");
			} else if (div5) {
				System.out.println("Buzz");
			} else {
				System.out.println(i);
			}
		}
	}
}