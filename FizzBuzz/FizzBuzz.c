/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <stdio.h>

int main()
{
    int i;
    int div3;
    int div5;

    for (i = 1; i <= 100; i++ ) {
        div3 = (i%3) == 0;
        div5 = (i%5) == 0;

        if (div3 && div5) {
            printf("FizzBuzz\n");
        } else if (div3) {
            printf("Fizz\n");
        } else if (div5) {
            printf("Buzz\n");
        } else {
            printf("%d\n", i);
        }
    }
    return 0;
}
