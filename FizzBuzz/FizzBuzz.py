'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

['FizzBuzz' if i%15 is 0 else 'Buzz' if i%5 is 0 else 'Fizz' if i%3 is 0 else i for i in range(1,101)]