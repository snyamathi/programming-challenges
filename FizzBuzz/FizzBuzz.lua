--[[
	Suneil Nyamathi
	snyamathi@gmail.com
]]

local mod3
local mod5

for i = 1, 100, 1 do
	mod3 = i%3 == 0
	mod5 = i%5 == 0
	
	if mod3 and mod5 then
		print("FizzBuzz")
	elseif mod3 then
		print("Fizz")
	elseif mod5 then
		print("Buzz")
	else
		print(i)
	end
end