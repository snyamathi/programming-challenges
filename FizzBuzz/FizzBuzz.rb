=begin
	Suneil Nyamathi
	snyamathi@gmail.com
=end

for i in 1..100
	div3 = i%3 == 0
	div5 = i%5 == 0

	if div3 and div5 then
		print "FizzBuzz\n"
	elsif div3 then
		print "Fizz\n"
	elsif div5 == 0 then
		print "Buzz\n"
	else
		print i, "\n"
	end
end