# Suneil Nyamathi
# snyamathi@gmail.com
	
	.data
fizz:	.asciiz "Fizz\n"
buzz:	.asciiz	"Buzz\n"
fizb:	.asciiz "FizzBuzz\n"
newl:	.asciiz "\n"
	.text
	
main:
	li	$t0,	0
	li	$t3,	3
	li	$t5,	5
	li	$t6,	15
	
loop:	
	beq	$t0,	100,	exit
	addi	$t0,	$t0,	1
	div	$t0,	$t6
	mfhi	$t1
	beq	$t1,	$zero,	printFizzBuzz
	div	$t0,	$t5
	mfhi	$t1
	beq	$t1,	$zero,	printBuzz
	div	$t0,	$t3
	mfhi	$t1
	beq	$t1,	$zero,	printFizz
	j	printInt

printInt:
	move	$a0,	$t0
	li,	$v0,	1
	syscall
	la	$a0,	newl
	li	$v0,	4
	syscall
	j	loop

printFizz:
	la	$a0,	fizz
	li	$v0,	4
	syscall
	j	loop
	
printBuzz:
	la	$a0,	buzz
	li	$v0,	4
	syscall
	j	loop
	
printFizzBuzz:
	la	$a0,	fizb
	li	$v0,	4
	syscall
	j	loop
	
exit: