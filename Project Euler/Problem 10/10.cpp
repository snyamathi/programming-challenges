/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

bool isPrime(int n)
{
	if (n == 2)
		return true;

    if (n % 2 == 0)
        return false;

    for (int i = 3; i*i <= n; i += 2) {
        if (n%i == 0)
            return false;
    }

    return true;
}

int main()
{
	long long sum = 0;
	int stop = 0;

	for (int i = 2; i < 2000000; i++) {
		if (isPrime(i)) {
			sum += i;
			stop++;
			if (stop == 100) {
				std::cout << i << std::endl;
			}
		}
	}

	std::cout << sum << std::endl;
	return 0;
}