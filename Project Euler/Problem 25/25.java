/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

import java.math.BigInteger;

public class P4 {
	
	private static BigInteger bigFib(int n) {
		
		if (n == 0)
			return new BigInteger("0");
		if (n == 1)
			return new BigInteger("1");
		
		BigInteger past = new BigInteger("0");
		BigInteger last = new BigInteger("1");
		BigInteger result = new BigInteger("0");
		
		for (int i = 2; i <= n; i++) {
			result = last.add(past);
			past = last;
			last = result;
		}
			
		return result;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 9999999; i++) {
			if (bigFib(i).toString().length() >= 1000) {
				System.out.println(i);
				return;
			}
		}
	}
}
