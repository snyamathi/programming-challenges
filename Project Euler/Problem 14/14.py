'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

max = 1000000
solved = {1:0}
queue = []

def fillQueue(n):
	queue.append(n)
	while True:
		if n in solved:
			return
		elif n%2 == 0:
			n = n/2
			queue.append(n)
		else:
			n = n*3+1
			queue.append(n)
	
def emptyQueue():
	count = solved[queue[len(queue)-1]]
	while len(queue) > 0:
		solved[queue.pop(len(queue)-1)] = count
		count += 1

for i in range (1, max):
	fillQueue(i)
	emptyQueue()

# print(solved)

maxKey = 0
maxValue = 0
for key in solved:
	if key < max and solved[key] > maxValue:
		maxKey = key
		maxValue = solved[key]
		
print(maxKey)