import itertools

# Turn a list of numbers into a number ( [1,2,3] -> 123 )
def toNumber(listOfNumber):
    concat = ''.join(map(str, listOfNumber))
    return int(concat)
    
# Check to see if a number meets the criteria of the problem
def isCompleteValidNumber(n):
    if toNumber(n[1:4]) %  2 != 0:
        return False        
    if toNumber(n[2:5]) %  3 != 0:
        return False    
    if toNumber(n[3:6]) %  5 != 0:
        return False    
    if toNumber(n[4:7]) %  7 != 0:
        return False    
    if toNumber(n[5:8]) % 11 != 0:
        return False    
    if toNumber(n[6:9]) % 13 != 0:
        return False    
    if toNumber(n[7:])  % 17 != 0:
        return False

    return True

# Holds a subset of the complete space of numbers to control problem size
# It was easy to figure out some restrictions (such as % 5) and then we reduce
# the problem size substantially
class ValidNumber:
    def __init__(self, d3, d4, d5, d6):
        self.d3 = d3
        self.d4 = d4
        self.d5 = d5
        self.d6 = d6
        
    def __str__(self):
        return str([-1,-1,self.d3,self.d4,self.d5,self.d6,-1,-1,-1,-1])
        
    def __repr__(self):
        return self.__str__()
        
    def getSumOfValidValues(self):
        retValue = 0
    
        numbers = range(10)
        numbers.remove(self.d3)
        numbers.remove(self.d4)
        numbers.remove(self.d5)
        numbers.remove(self.d6)
        
        for j in itertools.permutations(numbers, 6):
            l = []
            l.append(j[0])
            l.append(j[1])
            l.append(self.d3)
            l.append(self.d4)
            l.append(self.d5)
            l.append(self.d6)
            l.append(j[2])
            l.append(j[3])
            l.append(j[4])
            l.append(j[5])
            
            if isCompleteValidNumber(l):
                retValue += toNumber(l)
                
        return retValue

m = []        
        
# Start with the full range of numbers        
n = range(10)

# Get the numbers with the restriction of % 5

# d6 must be 0
n.remove(0)
r1 = [2, 4, 6, 8]
for i in r1:
    n.remove(i)
    
    for p in itertools.permutations(n, 2):
        if (p[0] + p[1] + i) % 3 == 0:
            m.append(ValidNumber(p[0], i, p[1], 0))
            
    n.append(i)
    n.sort()

n.append(0)
n.sort()

# or 5
n.remove(5)
r1 = [0, 2, 4, 6, 8]
for i in r1:
    n.remove(i)
    
    for p in itertools.permutations(n, 2):
        if (p[0] + p[1] + i) % 3 == 0:
            m.append(ValidNumber(p[0], i, p[1], 5))
            
    n.append(i)
    n.sort()

n.append(5)
n.sort()

# then brute force the rest
sum = 0
for i in m:
    sum += i.getSumOfValidValues()
    
print sum

