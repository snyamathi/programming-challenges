--[[
	Suneil Nyamathi
	snyamathi@gmail.com
]]

local function fib(n)
	
	if n == 0 then return 0 end
	if n == 1 then return 1 end
	
	local past = 0
	local last = 1
	local result = 0
	
	for i = 2,n do
		result = last + past
		past = last
		last = result
	end
	
	return result
end

local value = 0
local sum = 0

for i = 0,1000000 do
	value = fib(i)
	if value%2 == 0 then
		sum = sum + value
	end
	
	if value > 4000000 then break end
end

print(sum)

