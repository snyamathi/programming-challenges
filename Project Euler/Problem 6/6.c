/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <stdio.h>
#include <math.h>

int main()
{
    double i;
	double sum = 0;
	for(i = 1; i <= 100; i++) {
		sum += pow(i, 2);
	}

    double series = pow(100*(101)/2, 2);

	printf("%f", series - sum);
	return 0;
}