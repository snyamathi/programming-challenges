/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

bool isPrime(const long & factor)
{
    if (factor % 2 == 0)
        return false;

    for (int i = 3; i*i <= factor; i += 2) {
        if (factor%i == 0)
            return false;
    }

    return true;
}

int main()
{
	int i = 10000;
	long j = 1;

	while (i) {
		j++;
		if (isPrime(j))
			i--;
	}
	std::cout << j;
	return 0;
}