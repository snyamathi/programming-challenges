'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

coins = [1,2,5,10,20,50,100,200]

max = 200

ways = []

for i in range (0, len(coins)):
  ways.append([])
  
for y in range (0, len(coins)):
  coin = coins[y]
  for x in range (0, max+1):
    if coin == 1:
      ways[y].append(1)
    elif x >= coins[y]:
      ways[y].append(ways[y-1][x] + ways[y][x-coin])
    else:
      ways[y].append(ways[y-1][x])
  
  
print(ways[-1][-1])