--[[
	Suneil Nyamathi
	snyamathi@gmail.com
]]

local product = 0
local answer = 0

for i = 999,100,-1 do
	for j = 999,100,-1 do
		product = i * j
		if product == tonumber(string.reverse(product)) then
			if product > answer then
				answer = product
			end
		end
	end
end

print(answer)