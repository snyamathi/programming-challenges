/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

import java.math.BigInteger;

public class P4 {

	public static void main(String[] args) {
		BigInteger bigInt = new BigInteger("1");
		
		for (int i = 100; i > 0; i--) {
			bigInt = bigInt.multiply(new BigInteger("" + i));
		}
		
		int sum = 0;
		char[] numbers = bigInt.toString().toCharArray();
		for (char c : numbers) {
			sum += c - 48;
		}
		System.out.println(sum);
	}
}
