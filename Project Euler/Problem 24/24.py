'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

import math

n = 1000000
L = range(10)
M = []

for i in range (9, 0, -1):
  fact = math.factorial(i)
  times = n / fact
  n -= times*fact

  if n  == 0:
    M.append(L.pop(times-1))
    M += reversed(L)
    break
  else:
    M.append(L.pop(times))

print ''.join(str(i) for i in M)
