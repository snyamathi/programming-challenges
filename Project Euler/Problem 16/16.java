/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

import java.math.BigInteger;

public class P4 {

	public static void main(String[] args) {
		BigInteger bigInt = new BigInteger("2");
		bigInt = bigInt.pow(1000);
		
		int sum = 0;
		char[] numbers = bigInt.toString().toCharArray();
		for (char c : numbers) {
			sum += c - 48;
		}
		System.out.println(sum);
	}
}
