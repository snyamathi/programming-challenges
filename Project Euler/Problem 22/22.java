/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class P4 {

	public static void main(String[] args) {
		try {
			long answer = 0;
			BufferedReader reader = new BufferedReader(new FileReader("C:\\names.txt"));
			String inefficient = reader.readLine();
			inefficient = inefficient.replaceAll("\"", "");
			List<String> names = new ArrayList<String>();
			for (String name : inefficient.split(",")) {
				names.add(name);
			}
			
			Collections.sort(names);
			for (int i = 0; i < names.size(); i++) {
				String name = names.get(i);
				int sum = 0;
				for (char c : name.toCharArray()) {
					sum += c - 64;
				}
				answer += sum * (i+1);
			}
			System.out.println(answer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}