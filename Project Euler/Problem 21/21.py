'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

def d(n):
  divs = [1]
  for i in range (2, n/2 + 1):
    if n % i == 0:
      divs.append(i)
  return sum(divs)

l = []

for a in range (1, 10001):
  b = d(a)
  if d(b) == a and a != b:
    l.append(a)

print sum(l)