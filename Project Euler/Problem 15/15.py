'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

max = 21

array = []

for i in range (max):
  array.append([])
  for j in range (max):
    array[i].append(1)
  
for i in range (1, max):
  x = i
  y = i
  
  for j in range (i, max):
    array[y][j] = array[y][j-1] + array[y-1][j]
    array[j][x] = array[j-1][x] + array[j][x-1]
  
print(array[max-1][max-1])
