/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

bool isDivisible(long n)
{
	for (int i = 2; i <= 20; i++) {
		if (n%i != 0) {
			return false;
		}
	}
	return true;
}

int main()
{
	int answer;
	for (long i = 20; ; i = i + 20) {
		if (isDivisible(i)) {
			answer = i;
			break;
		}
	}
	std::cout << answer;
	return 0;
}