'''
	Suneil Nyamathi
	snyamathi@gmail.com
'''

from math import sqrt

a = 0
b = 0
c = 0

for a in range(0,333):
	for b in range(1,500):
		if a >= b:
			continue
		c = sqrt(a*a + b*b)
		if a + b + c == 1000:
			print(a*b*c)
			break