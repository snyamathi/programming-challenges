/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

int numberOfDivisors(long n)
{
	int count = 0;
	for (long i = 1; i*i <= n; i++) {
		if (n%i == 0)
			count += 2;
	}
	return count;
}

int main()
{
	long j = 1;
	for (long i = 1; ; i += ++j) {
		if (numberOfDivisors(i) > 500) {
			std::cout << i << std::endl;
			return 0;
		}
	}
	return 1;
}