/*
 * Suneil Nyamathi
 * snyamathi@gmail.com
 */

#include <iostream>

bool isPrime(const int & factor)
{
    if (factor % 2 == 0)
        return false;

    for (int i = 3; i*i < factor; i += 2) {
        if (factor%i == 0)
            return false;
    }

    return true;
}

int main()
{
	long long num = 600851475143L;
	int sqrtOfNum = 775147; // We can't have a factor larger than the sqrt of the number

	for (int i = sqrtOfNum; i > 2; i -= 2) {
		if ((num%i) == 0 && isPrime(i)) {
			std::cout << i;
			return 0;
		}
	}
	return 1;
}